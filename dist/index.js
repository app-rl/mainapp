export default {
  install(Vue, options = {}) {
    options.debug = options.debug || false
    options.timeout = options.timeout || 2000

    window.GetActionFromApp = function(type, event, debug = false) {
      const e = new CustomEvent("mainapp", {
        detail: {
          type: type,
          event: event,
          debug: debug
        }
      })

      if (options.debug === true) {
        alert(`${type} : ` + JSON.stringify(event))
      }

      window.dispatchEvent(e)
    }

    window.MainApp = Vue.prototype.MainApp = new Vue({
      data: () => {
        return {
          checkIfReturnCallback: null,
          features: {
            ScanBarcodeFromCamera: false,
            ScanBarcodeFromScanner: false,
            GetLocation: false,
            GetVoiceText: false,
            GetReaderNFC: false,
            GetOneSignalPlayerID: false,
            GetTouchID: false
          },
          allFeatures: {
            ScanBarcodeFromCamera: {
              value: { status: 'success', value: '7290000001234' }
            },
            ScanBarcodeFromScanner: {
              trigger: false,
              value: { status: 'success', value: '7290000001234' }
            },
            GetLocation: {
              value: {
                status: 'success',
                value: {
                  lat: '000000',
                  long: '00000'
                }
              }
            },
            GetVoiceText: {
              value: { status: 'success', value: 'לחם' }
            },
            GetReaderNFC: {
              value: { status: 'success', value: 'ObjectFromNFC-?' }
            },
            GetOneSignalPlayerID: {
              value: { status: 'success', value: 'ONESIGNALUSERID' }
            },
            GetTouchID: {
              value: { status: 'success', value: 'GetTouchID-?' }
            }
          },
          result: null
        }
      },
      mounted() {
        window.addEventListener("mainapp", this.onMainApp)
      },
      methods: {
        GetActionFromApp(...args) {
          return window.GetActionFromApp(...args)
        },
        init(arg) {
          this.features = Object.assign(this.features, arg || {})
        },
        onMainApp(e) {
          // parent.location.hash = "reset"
          this.result = e
        },
        isCanFeature(feature) {
          return this.features[feature] || false
        },
        async action(url, params = {}) {
          if (this.checkIfReturnCallback) {
            clearInterval(this.checkIfReturnCallback)
          }
          return new Promise((resolve, reject) => {
            let times = 1
            this.result = null
            // parent.location.hash = url

            parent.postMessage(JSON.stringify({
                event: 'MainAppEvent',
                action: url,
                params: params
            }), '*');

            this.checkIfReturnCallback = setInterval(() => {
              if (this.result) {
                clearInterval(this.checkIfReturnCallback);
                return resolve(this.result);
              } else if (times > 50) {

                // parent.location.hash = "timeout";
                clearInterval(this.checkIfReturnCallback);

                return reject({
                  status: "timeout",
                  value: "timeout"
                });
              } else {
                times++;
              }
            }, (options.timeout/10))
          })
        }
      }
    })
  }
}
